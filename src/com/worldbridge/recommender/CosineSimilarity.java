package com.worldbridge.recommender;

import java.util.List;

/**
 * Created by Pham Phi Long on 6/23/2015.
 */
public class CosineSimilarity<E extends Number> implements Similarity<E> {
    @Override
    public Double calculateSimilarity(List<E> vector1, List<E> vector2, int dimensionNum) {
        if (null == vector1 || null == vector2) {
            return null;
        } else if (vector1.size() < dimensionNum || vector1.size() < dimensionNum) {
            return null;
        }

        Double cosineSimilarity;
        Double vector1Magnitude = new Double(0.0);
        Double vector2Magnitude = new Double(0.0);
        Double dotProduct = new Double(0.0);

        for (int i=0; i<dimensionNum; i++) {
            Double v1i = (Double)vector1.get(i);
            Double v2i = (Double)vector2.get(i);

            if (v1i != null && v2i != null) {
                dotProduct += v1i * v2i;
            }

            if (v1i != null) {
                vector1Magnitude += v1i * v1i;
            }

            if (v2i != null) {
                vector2Magnitude += v2i * v2i;
            }
        }

        vector1Magnitude = Math.sqrt(vector1Magnitude.doubleValue());
        vector2Magnitude = Math.sqrt(vector2Magnitude.doubleValue());

        cosineSimilarity = dotProduct / (vector1Magnitude * vector2Magnitude);

        return cosineSimilarity;
    }
}

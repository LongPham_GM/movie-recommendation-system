package com.worldbridge.recommender;

import java.util.List;

/**
 * Created by Pham Phi Long on 6/23/2015.
 */
public interface Similarity<E extends Number> {
    Double calculateSimilarity(List<E> vector1, List<E> vector2, int dimensionNum);
}

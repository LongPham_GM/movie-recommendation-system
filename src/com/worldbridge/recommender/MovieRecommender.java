package com.worldbridge.recommender;

import java.util.*;

/**
 * Created by Pham Phi Long on 6/23/2015.
 */
public class MovieRecommender {
    private HashMap<String, Integer> usersPosition = new HashMap<>();
    private HashMap<String, Integer> moviePosition = new HashMap<>();
    private List<Rating.User> users = new ArrayList<>();
    private List<Rating.Movie> movies = new ArrayList<>();
    private List<List<Double>> utilityMatrix = new ArrayList<>(); // Map of film rating by users
    private final Similarity<Double> similarityCalculator;
    private final int knn;

    static private final int DEFAULT_KNN = 10;

    public MovieRecommender(Similarity<Double> similarityCalculator, int knn) {
        this.similarityCalculator = similarityCalculator;
        this.knn = (knn > 0) ? knn : DEFAULT_KNN;
    }

    public void addRating(Rating rating) {
        String userName = rating.getUser().getUserName();
        String movieName = rating.getMovie().getMovieName();

        if (!usersPosition.containsKey(userName)) {
            users.add(rating.getUser());
            usersPosition.put(userName, users.size() - 1);
            utilityMatrix.add(new ArrayList<>());

            List<Double> newUserRatingList = utilityMatrix.get(utilityMatrix.size() -1);
            for (int i=0; i< movies.size(); i++) {
                newUserRatingList.add(null);
            }
        }

        if (!moviePosition.containsKey(movieName)) {
            movies.add(rating.getMovie());
            moviePosition.put(movieName, movies.size() - 1);

            for (int i=0; i<users.size(); i++) {
                utilityMatrix.get(i).add(null);
            }
        }

        utilityMatrix.get(usersPosition.get(userName)).set(moviePosition.get(movieName), rating.getRating());
    }

    public List<Rating> getRecommendedMovieList(String userName, int numOfMovie) {
        if (!usersPosition.containsKey(userName) || numOfMovie <= 0) {
            return null;
        }

        PriorityQueue<Rating> recommendedMovieQueue = new PriorityQueue<>(new Rating.Comparator());
        List<Double> userRatingList = utilityMatrix.get(usersPosition.get(userName));
        PriorityQueue<UserRatingSimilarity> similarityOrderList = getSimilarUserList(userName);

        for (Rating.Movie movie : movies) {
            String movieName = movie.getMovieName();

            if (userRatingList.get(moviePosition.get(movieName)) == null) {
                Double predictedRating = predictRatingForMovie(movieName, similarityOrderList, 0.0);
                recommendedMovieQueue.add(new Rating(userName, movieName, predictedRating));
            }
        }

        numOfMovie = (numOfMovie < recommendedMovieQueue.size()) ? numOfMovie : recommendedMovieQueue.size();
        List<Rating> recommendedMovieList = new ArrayList<>();

        for (int i=0; i<numOfMovie; i++) {
            recommendedMovieList.add(recommendedMovieQueue.remove());
        }

        return recommendedMovieList;
    }

    public Double getRatingOf(String userName, String movieName) {
        final boolean isUserExisted = usersPosition.containsKey(userName);
        final boolean isMovieExisted = moviePosition.containsKey(movieName);
        final int userPosition = usersPosition.get(userName);
        Double userNormalizationCoefficient = 0.0;

        if (!isUserExisted || !isMovieExisted) {
            return null;
        } else {
            Double ratingValue = utilityMatrix.get(usersPosition.get(userName)).get(moviePosition.get(movieName));

            if (ratingValue != null) return ratingValue;
        }

        // normalize each user rating set
        Double globalAverageRating = calcGlobalAverageRating(utilityMatrix);

        for (int i=0; i<utilityMatrix.size(); i++) {
            Double normalizationCoefficient = calcNormalizationRatingCoefficient(utilityMatrix.get(i), globalAverageRating);
            if (i == userPosition) {       // save the normalization coefficient of user for later use
                userNormalizationCoefficient = normalizationCoefficient;
            }
        }

        PriorityQueue<UserRatingSimilarity> similarityOrderList = getSimilarUserList(userName);

        return predictRatingForMovie(movieName, similarityOrderList, userNormalizationCoefficient);
    }

    private Double predictRatingForMovie(String movieName, PriorityQueue<UserRatingSimilarity> similarityOrderList, Double userNormalizationCoefficient) {
        Double predictedRating = 0.0;
        int similarityListSize = similarityOrderList.size();
        int legalRating = 0;

        for (int i=0; i<similarityListSize && legalRating<knn; i++) {
            UserRatingSimilarity similarity = similarityOrderList.remove();
            Rating.User otherUser = similarity.getUserB();
            Double otherUserRating = utilityMatrix.get(usersPosition.get(otherUser.getUserName())).get(moviePosition.get(movieName));
            if (otherUserRating != null) {
                predictedRating += otherUserRating;
                legalRating++;
            }
        }
        predictedRating = (legalRating != 0) ? predictedRating / legalRating : 0.0;
        predictedRating += userNormalizationCoefficient;

        return predictedRating;
    }

    private PriorityQueue<UserRatingSimilarity> getSimilarUserList(String userName) {
        final Rating.User user = new Rating.User(userName);
        final int userPosition = usersPosition.get(userName);
        PriorityQueue<UserRatingSimilarity> similarityOrderList = new PriorityQueue<>(new UserRatingSimilarity.Comparator());

        List<Double> userRatingList = utilityMatrix.get(userPosition);
        for (int i=0; i<utilityMatrix.size(); i++) {
            if (i == userPosition) continue;

            List<Double> otherUserRatingList = utilityMatrix.get(i);
            Double similarity = similarityCalculator.calculateSimilarity(userRatingList, otherUserRatingList, movies.size());
            if (similarity != null) {
                similarityOrderList.add(new UserRatingSimilarity(user, users.get(i), similarity));
            }
        }

        return  similarityOrderList;
    }

    private Double calcGlobalAverageRating(List<List<Double>> ratingMatrix) {
        Double globalRating = 0.0;
        int numOfExistedRating = 0;

        for (List<Double> ratingList : ratingMatrix) {
            for (Double rating : ratingList) {
                if (rating != null) {
                    globalRating += rating;
                    numOfExistedRating++;
                }
            }
        }

        return (numOfExistedRating != 0) ? (globalRating / numOfExistedRating) : 0;
    }

    private Double calcNormalizationRatingCoefficient (List<Double> ratingList, Double globalAverageRating) {
        Double normalizationCoefficient = 0.0;
        Double averageRating = 0.0;
        int numOfExistedRating = 0;

        for (Double rating : ratingList) {
            if (rating != null) {
                averageRating += rating;
                numOfExistedRating++;
            }
        }

        if (numOfExistedRating != 0) {
            averageRating /= numOfExistedRating;
        }

        normalizationCoefficient = averageRating - globalAverageRating;
        return normalizationCoefficient;
    }

    public static class Rating {
        private final User user;
        private final Movie movie;
        private final Double ratingValue;

        public Rating(String userName, String movieName, Double ratingValue) {
            this.user = new User(userName);
            this.movie = new Movie(movieName);
            this.ratingValue = ratingValue;
        }

        public User getUser() {
            return user;
        }

        public Movie getMovie() {
            return movie;
        }

        public Double getRating() {
            return ratingValue;
        }

        static public class User {
            private final String userName;

            public User(String user) {
                this.userName = user;
            }

            @Override
            public boolean equals(Object obj) {
                if (obj instanceof User) {
                    User other = (User) obj;
                    return this.userName.equals(other.userName);
                } else {
                    return false;
                }
            }

            @Override
            public int hashCode(){
                return this.userName.hashCode();
            }

            public String getUserName() {
                return userName;
            }
        }

        static public class Movie {
            private final String movieName;

            public Movie(String user) {
                this.movieName = user;
            }

            @Override
            public boolean equals(Object obj) {
                if (obj instanceof Movie) {
                    Movie other = (Movie) obj;
                    return this.movieName.equals(other.movieName);
                } else {
                    return false;
                }
            }

            @Override
            public int hashCode(){
                return this.movieName.hashCode();
            }

            public String getMovieName() {
                return movieName;
            }
        }

        static class Comparator implements java.util.Comparator<Rating> {

            @Override
            public int compare(Rating o1, Rating o2) {
                if (o1.getRating() < o2.getRating()) return 1;
                if (o1.getRating() > o2.getRating()) return -1;
                return 0;
            }
        }
    }

    static private class UserRatingSimilarity {
        public Rating.User getUserA() {
            return userA;
        }

        public Rating.User getUserB() {
            return userB;
        }

        private final Rating.User userA;
        private final Rating.User userB;
        private final Double similarity;

        public Double getSimilarity() {
            return similarity;
        }

        UserRatingSimilarity(Rating.User userA, Rating.User userB, Double similarity) {
            this.userA = userA;
            this.userB = userB;
            this.similarity = similarity;
        }

        static class Comparator implements java.util.Comparator<UserRatingSimilarity> {

            @Override
            public int compare(UserRatingSimilarity o1, UserRatingSimilarity o2) {
                if (o1.similarity < o2.similarity) return 1;
                if (o1.similarity > o2.similarity) return -1;
                return 0;
            }
        }
    }
}

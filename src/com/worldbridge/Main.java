package com.worldbridge;

import com.worldbridge.movie_database.MovieDatabaseHelper;
import com.worldbridge.recommender.CosineSimilarity;
import com.worldbridge.recommender.MovieRecommender;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class Main {

    public static void main(String[] args) {
        MovieRecommender movieRecommender = new MovieRecommender(new CosineSimilarity<>(), 10);

        if (BuildOption.USE_REAL_DATABASE) {
            MovieDatabaseHelper movieDatabaseParser = new MovieDatabaseHelper(";");

            // parse the database
            List<MovieRecommender.Rating> databaseRatingList = movieDatabaseParser.parseFile("database\\trimmed_off_database.txt");
            for (MovieRecommender.Rating rating : databaseRatingList) {
                movieRecommender.addRating(rating);
            }

            // compare predicted rating with real rating
            List<MovieRecommender.Rating> testDatabaseRatingList = movieDatabaseParser.parseFile("database\\test_database.txt");
            List<List<MovieDatabaseHelper.RatingEncodingInfo>> encodingInfoList = new ArrayList<>();

            for (MovieRecommender.Rating testedRating : testDatabaseRatingList) {
                Double predictedRatingValue = movieRecommender.getRatingOf(testedRating.getUser().getUserName(), testedRating.getMovie().getMovieName());
                List<MovieDatabaseHelper.RatingEncodingInfo> ratingInfoList = new ArrayList<>();
                MovieDatabaseHelper.RatingEncodingInfo ratingInfo = new MovieDatabaseHelper.RatingEncodingInfo(testedRating.getUser().getUserName(), testedRating.getMovie().getMovieName(), testedRating.getRating(), predictedRatingValue);
                ratingInfoList.add(ratingInfo);
                encodingInfoList.add(ratingInfoList);
            }
            movieDatabaseParser.encodeFile("report\\test_predicted_rating.report", encodingInfoList);

            // get recommended movie list
            List<List<MovieDatabaseHelper.RatingEncodingInfo>> recommendedMovieForUserList = new ArrayList<>();
            for (MovieRecommender.Rating testedRating : testDatabaseRatingList) {
                List<MovieRecommender.Rating> recommendedMovieList = movieRecommender.getRecommendedMovieList(testedRating.getUser().getUserName(), 10);
                List<MovieDatabaseHelper.RatingEncodingInfo> ratingInfoList = new ArrayList<>();

                for (MovieRecommender.Rating rating : recommendedMovieList) {
                    MovieDatabaseHelper.RatingEncodingInfo ratingInfo = new MovieDatabaseHelper.RatingEncodingInfo(rating.getUser().getUserName(), rating.getMovie().getMovieName(), null, rating.getRating());
                    ratingInfoList.add(ratingInfo);
                }

                recommendedMovieForUserList.add(ratingInfoList);
            }
            movieDatabaseParser.encodeFile("report\\recommended_movie.report", recommendedMovieForUserList);

        } else {
            // this is the example in the book
            movieRecommender.addRating(new MovieRecommender.Rating("A", "HP1", 4.0));
            movieRecommender.addRating(new MovieRecommender.Rating("A", "TW", 5.0));
            movieRecommender.addRating(new MovieRecommender.Rating("A", "SW1", 1.0));
            movieRecommender.addRating(new MovieRecommender.Rating("B", "HP1", 5.0));
            movieRecommender.addRating(new MovieRecommender.Rating("B", "HP2", 5.0));
            movieRecommender.addRating(new MovieRecommender.Rating("B", "HP3", 4.0));
            movieRecommender.addRating(new MovieRecommender.Rating("C", "TW", 2.0));
            movieRecommender.addRating(new MovieRecommender.Rating("C", "SW1", 4.0));
            movieRecommender.addRating(new MovieRecommender.Rating("C", "SW2", 5.0));
            movieRecommender.addRating(new MovieRecommender.Rating("D", "HP2", 3.0));
            movieRecommender.addRating(new MovieRecommender.Rating("D", "SW3", 3.0));

            Double testRating = movieRecommender.getRatingOf("B", "TW");
        }
    }
}

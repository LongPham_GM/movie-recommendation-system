package com.worldbridge.movie_database;

import com.worldbridge.recommender.MovieRecommender;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Pham Phi Long on 6/27/2015.
 */
public class MovieDatabaseHelper {
    private final String fieldSplitter;
    private enum FieldPosition {
        USER(0),
        MOVIE(1),
        RATING(2),
        PREDICTED_RATING(3);

        private int position;
        private FieldPosition(int position) {
            this.position = position;
        }
    }

    public MovieDatabaseHelper(String fieldSpiltter) {
        this.fieldSplitter = fieldSpiltter;
    }

    public List<MovieRecommender.Rating> parseFile(String fileName) {
        List<MovieRecommender.Rating> ratingList = new ArrayList<>();

        try {
            File f = new File(fileName);
            Scanner sc = new Scanner(f);

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] fields = line.split(fieldSplitter);
                String user = fields[FieldPosition.USER.position];
                String movie = fields[FieldPosition.MOVIE.position];
                Double rating = Double.parseDouble(fields[FieldPosition.RATING.position]);
                MovieRecommender.Rating p = new MovieRecommender.Rating(user, movie, rating);
                ratingList.add(p);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return ratingList;
    }

    public void encodeFile(String fileName, List<List<RatingEncodingInfo>> encodingInfoList) {
        try {
            FileWriter fw = new FileWriter(fileName);
            PrintWriter out = new PrintWriter(fw);

            for (List<RatingEncodingInfo> userInfo : encodingInfoList) {
                out.println("**********************************************************");
                out.println("User: " + userInfo.get(0).getUserName());
                for (RatingEncodingInfo ratingInfo : userInfo) {
                    out.println(ratingInfo.getMovieName() + fieldSplitter + '\t' + ratingInfo.getRating() + fieldSplitter + "\t" + ratingInfo.getPredictedRating());
                }
            }

            out.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    static public class RatingEncodingInfo {
        private final String userName;
        private final String movieName;
        private final Double rating;
        private final Double predictedRating;

        public RatingEncodingInfo(String userName, String movieName, Double rating, Double predeictedRating) {
            this.userName = userName;
            this.movieName = movieName;
            this.rating = rating;
            this.predictedRating = predeictedRating;
        }

        public String getUserName() {
            return userName;
        }

        public String getMovieName() {
            return movieName;
        }

        public Double getRating() {
            return rating;
        }

        public Double getPredictedRating() {
            return predictedRating;
        }
    }
}
